#!/usr/bin/env bash

docker-compose -f docker-compose.init.yml run --rm init && \
    docker-compose up -d --build > build-log.txt

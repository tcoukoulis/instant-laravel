#!/usr/bin/env bash

if [[ "$#" -lt "1" ]]; then
    echo INIT SERVICE: script requires 1 argument, "$#" arguments provided, aborting...
    exit 1
fi

src_dir="$1/src"

if [[ ! -d "$src_dir" ]]; then
    echo INIT SERVICE: No src directory detected, initializing new application...
    laravel new src
    exit 0
else
    echo INIT SERVICE: src directory exists, skipping application initialization...
    exit 1
fi
